#!/usr/bin/env python3
import sys
import time
import argparse
from pathlib import Path
from itertools import zip_longest


def directory(string):
    path = Path(string)
    if path.is_dir():
        return path
    else:
        raise NotADirectoryError(string)

def file(string):
    path = Path(string)
    if path.is_file():
        return path
    else:
        raise RuntimeError(string)

def main():
    parser = argparse.ArgumentParser(description='Merge distance files - 2.')
    parser.add_argument('length_list', type=file)
    parser.add_argument('input_dir', type=directory)
    args = parser.parse_args()

    stime = time.time()
    with open(args.length_list, "rt") as fin:
        lengths = [s.strip() for s in fin]

    for i, l in enumerate(lengths):
        if l == '1': continue
        fin_name = args.input_dir / f"{l}.all.res"

        with open(fin_name, "rt") as fin:
            for line in fin:
                print(l + "\t" + line.rstrip())

main()
