#!/usr/bin/env python3
import sys
import time
import argparse
from pathlib import Path
import numpy as np
import pandas as pd
from toolz.curried import *

import panel as pn
import matplotlib.pylab as plt

def file(string):
    path = Path(string)
    if path.is_file():
        return path
    else:
        raise RuntimeError(string)

def plot(source, length, data_chains, data_pdb):
    plt.close('all')
    if source == "Chains":
        df = data_chains.loc[length, ["v1", "v2"]]
    else:
        df = data_pdb.loc[length, ["v1", "v2"]]
    l1, h1 = df["v1"].quantile([0.01, 0.99])
    l2, h2 = df["v2"].quantile([0.01, 0.99])

    if h1 - l1 < h2 - l2:
        bins = (50, 50*int((h2 - l2)/(h1 - l1)))
    else:
        bins = (50*int((h1 - l1)/(h2 - l2)), 50)

    fig, ax = plt.subplots(figsize=(18, 8))
    _, _, _, im = ax.hist2d(x=df.v1, y=df.v2, range=((l1, h1), (l2, h2)), cmap="autumn", bins=bins, cmin=1)
    ax.axis("scaled")
    ax.set_title(f"{source} of length {length}.")
    fig.colorbar(im, location="bottom", ax=ax)
    return fig


def main():
    parser = argparse.ArgumentParser(description='Visualize data.')
    parser.add_argument('data_chains', type=file)
    parser.add_argument('data_pdb', type=file)
    args = parser.parse_args()

    data_chains = pd.read_csv(args.data_chains, sep='\t',
                              names=["pdb", "model", "chain", "len", "v1", "v2", "v3"], dtype={"model": str})\
        .sort_values("len").set_index("len")
    len_chains = list(data_chains.groupby(level="len").size().pipe(lambda s: s[s > 5]).index.drop(2).sort_values())

    data_pdb = pd.read_csv(args.data_pdb, sep="\t",
                             names=["pdb", "model", "chain", "len", "v1", "v2", "v3"], dtype={"model": str})\
        .sort_values("len").set_index("len")
    len_pdb = list(data_pdb.groupby(level="len").size().pipe(lambda s: s[s > 5]).index.drop(2).sort_values())

    length = pn.widgets.DiscreteSlider(name='Length', options=len_chains)
    source = pn.widgets.RadioButtonGroup(name='Data source', options=['PDB', 'Chains'], value="Chains", button_type='primary')

    def source_changed(ev):
        if ev.new == "PDB":
            length.options = len_pdb
            length.value = len_pdb[0]
        else:
            length.options = len_chains
            length.value = len_chains[0]
    source.param.watch(source_changed, 'value', onlychanged=False)
    hist = pn.bind(plot, source=source, length=length, data_chains=data_chains, data_pdb=data_pdb)

    pn.Column(source, length, hist).show(port=9797, address="127.0.0.1")


main()
