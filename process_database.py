#!/usr/bin/env python3
import sys
import gzip
import time
from pathlib import Path
from toolz.curried import *
from joblib import Parallel, delayed

from Bio.PDB import PDBParser

DATABASE_PATH = Path("/mnt/sdb1/work/proteins/database/")

stime = time.time()
print(f"Start time: {stime}", file=sys.stderr)
files = [f for folder in DATABASE_PATH.iterdir() for f in folder.iterdir()]
N_files = len(files)
print(f"There are {N_files} files.", file=sys.stderr)

def process(f):
    result, errors = [], []
    pdb_id = f.name[3:7]
    parser = PDBParser(QUIET=True)
    fin = gzip.open(f, "rt")
    struct = parser.get_structure(pdb_id, fin)
    fin.close()

    for model in struct:
        model_id = model.get_id()
        for chain in model:
            error = False
            chain_id = chain.get_id()
            chain_atoms = []
            for resid in chain:
                # we are looking for only ATOMs not HEATOMs
                if resid.get_id()[0] != ' ': continue
                main_atom = None
                for atom in resid:
                    if atom.get_id() == 'CA':
                        #assert not atom.is_disordered()
                        if main_atom is not None:
                            error = True
                            break
                        main_atom = atom
                if error or main_atom is None:
                    error = True
                    break
                chain_atoms.append(",".join(map(str, main_atom.get_coord())))
            if not error:
                chain_str = '\t'.join(chain_atoms)
                result.append(f"{pdb_id}\t{model_id}\t{chain_id}\t{len(chain_atoms)}\t{chain_str}")
            else:
                errors.append((pdb_id, model_id, chain_id))
    return result, errors

all_errors = []
for itr, batch in enumerate(partition_all(1000, files)):
    print(f"[{itr * 1000} / {N_files}]", file=sys.stderr)
    for result, errors in Parallel(n_jobs=5, verbose=0)(delayed(process)(f) for f in batch):
        all_errors.extend(errors)
        for r in result:
            print(r)

ftime = time.time()
print(f"Finish time: {ftime}, elapsed {ftime-stime}.", file=sys.stderr)
with open("./error_list.txt", "wt") as fout:
    for pdb_id, model_id, chain_id in all_errors:
        print(f"{pdb_id}\t{model_id}\t{chain_id}", file=fout)
