#!/usr/bin/env python3
import sys
import time
import argparse
import numpy as np
from pathlib import Path
from itertools import combinations
from toolz.curried import *
from scipy.spatial.distance import pdist, cdist, squareform

from joblib import Parallel, delayed

def directory(string):
    path = Path(string)
    if path.is_dir():
        return path
    else:
        raise NotADirectoryError(string)

def process_file(f, metric, output_dir):
    ready_file = output_dir / (f.name.split(".")[0] + "." + metric + ".done")
    if ready_file.exists():
        return
    start_time = time.time()
    chains, vectors, rvectors = [], [], []
    N = None
    with open(f, "rt") as fin:
        for l in fin:
            parts = l.split("\t")
            assert len(parts) == 6
            chains.append(tuple(parts[:3]))
            N = int(parts[3])

            pdists = np.fromstring(parts[4], dtype=np.float64, sep=",")
            assert len(pdists) == N*(N-1) // 2
            if metric == 'sorted':
                pdists.sort()
            vectors.append(pdists)

            if metric == "both":
                pdists = np.fromstring(parts[5].strip(), dtype=np.float64, sep=",")
                assert len(pdists) == N*(N-1) // 2
                rvectors.append(pdists)

    if N == 1:
        with open(output_dir / (f.name.split(".")[0] + "." + metric + ".stats"), "wt") as fout:
            print(f"{time.time() - start_time}\t{len(chains)}", file=fout)
        ready_file.touch()
        return

    vectors = np.array(vectors)
    pdists = pdist(vectors, 'chebyshev')

    fout = open(output_dir / (f.name.split(".")[0] + "." + metric + ".res"), "wt")
    if metric == "both":
        rvectors = np.array(rvectors)
        rpdists = squareform(cdist(vectors, rvectors, 'chebyshev'), checks=False)

        for inds, d, rd in zip(combinations(range(len(vectors)), 2), pdists, rpdists):
            res = min(d, rd)
            print("\t".join(chains[inds[0]]) + "\t" + "\t".join(chains[inds[1]]) + "\t" + str(res), file=fout)
    else:
        for inds, d in zip(combinations(range(len(vectors)), 2), pdists):
            print("\t".join(chains[inds[0]]) + "\t" + "\t".join(chains[inds[1]]) + "\t" + str(d), file=fout)
    fout.close()

    with open(output_dir / (f.name.split(".")[0] + "." + metric + ".stats"), "wt") as fout:
        print(f"{time.time() - start_time}\t{len(chains)}", file=fout)
    ready_file.touch()


def main():
    parser = argparse.ArgumentParser(description='Compute pairwise distances.')
    parser.add_argument('metric', choices=['direct', 'both', 'sorted'])
    parser.add_argument('input_dir', type=directory)
    parser.add_argument('output_dir', type=directory)
    args = parser.parse_args()

    files = [f for f in args.input_dir.iterdir() if f.name.endswith(".txt")]
    N = len(files)
    for idx, f in enumerate(files):
        print(f"[{idx+1} / {N}] {f}")
        process_file(f, args.metric, args.output_dir)

main()
