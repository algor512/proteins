#!/usr/bin/env python3
import sys
import time
from warnings import simplefilter
import argparse
from pathlib import Path
import numpy as np
from toolz.curried import *
from joblib import Parallel, delayed

simplefilter('error')

def file(string):
    path = Path(string)
    if path.is_file():
        return path
    else:
        raise RuntimeError(string)

def compute(line):
    parts = line.split("\t", 4)
    prefix_str = "\t".join(parts[:4])
    if parts[3] in ('0', '1'):
        return f"{prefix_str}\t\t\t\t"
    data = np.fromstring(parts[4].replace("\t", ","), dtype=np.float64, sep=",").reshape(-1, 3).T
    data_centered = (data - data.mean(axis=1).reshape(3, 1))
    cov = np.dot(data_centered, data_centered.T) / (data.shape[1] - 1)
    eig, _ = np.linalg.eig(cov)
    if not np.allclose(np.abs(np.imag(eig)), 0):
        raise RuntimeError("There are complex eigenvalues!")
    eig = np.real(eig)
    eig.sort()

    eig_str = "\t".join(np.char.mod('%.5f', eig))
    cov_str = ",".join(np.char.mod('%.5f', cov.reshape(-1)))
    return f"{prefix_str}\t{eig_str}\t{cov_str}"

def main():
    parser = argparse.ArgumentParser(description='Merge distance files - 2.')
    parser.add_argument('data', type=file)
    args = parser.parse_args()

    stime = time.time()
    with open(args.data, "rt") as fin:
        for out in map(compute, fin):
            print(out)
        # for itr, batch in enumerate(partition_all(1000, fin)):
        #     print(f"{itr * 1000}", file=sys.stderr)
        #     for output in Parallel(n_jobs=5, verbose=0)(delayed(compute)(l) for l in batch):
        #         print(output)
    print(f"Elapsed {time.time()-stime}.", file=sys.stderr)

main()
