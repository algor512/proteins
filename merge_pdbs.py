#!/usr/bin/env python3
import sys
import time
import argparse
from pathlib import Path
import numpy as np
import pandas as pd
from toolz.curried import *

def file(string):
    path = Path(string)
    if path.is_file():
        return path
    else:
        raise RuntimeError(string)

def main():
    parser = argparse.ArgumentParser(description='Merge PDB chains into single chain')
    parser.add_argument('data', type=file)
    args = parser.parse_args()

    key = None
    items = []
    with open(args.data, "rt") as fin:
        for l in fin:
            parts = l.rstrip().split("\t")
            if key is not None and (parts[0], parts[1]) != key:
                items_str = "\t".join(items)
                print(f"{key[0]}\t{key[1]}\t_\t{len(items)}\t{items_str}")
                items = []

            key = (parts[0], parts[1])
            items.extend(parts[4:])
    print(f"{key[0]}\t{key[1]}\t_\t{len(items)}\t{items_str}")

main()
