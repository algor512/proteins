#!/usr/bin/env python3
import sys
import time
import argparse
import requests
from functools import cache
from pathlib import Path
from itertools import zip_longest


def directory(string):
    path = Path(string)
    if path.is_dir():
        return path
    else:
        raise NotADirectoryError(string)

def file(string):
    path = Path(string)
    if path.is_file():
        return path
    else:
        raise RuntimeError(string)

@cache
def get_pub(pdb_id):
    url = f"https://files.rcsb.org/download/{pdb_id}.pdb"
    res = requests.get(url)

    auth, title, doi = [], [], []
    for l in res.text.splitlines():
        if l[:4] != 'JRNL': continue
        typ = l[12:16]
        if typ == 'AUTH':
            auth.append(l[19:].rstrip())
        elif typ == 'TITL':
            title.extend(l[19:].rstrip().split())
        elif typ == 'DOI ':
            doi.append(l[19:].rstrip())
    return " ".join(auth), " ".join(title), " ".join(doi)



def main():
    parser = argparse.ArgumentParser(description='Add publication infos')
    parser.add_argument('file', type=file)
    args = parser.parse_args()

    with open(args.file, "rt") as fin:
        for l in fin:
            parts = l.rstrip().split("\t")
            pdb1, pdb2 = parts[1], parts[4]
            auth1, title1, doi1 = get_pub(pdb1)
            auth2, title2, doi2 = get_pub(pdb2)
            format1 = f"{title1}, doi: {doi1}"
            format2 = f"{title2}, doi: {doi2}"
            print(l.rstrip() + "\t" + format1 + "\t" + format2)

    # for i, l in enumerate(lengths):
    #     if l == '1': continue
    #     fin_name = args.input_dir / f"{l}.all.res"

    #     with open(fin_name, "rt") as fin:
    #         for line in fin:
    #             print(l + "\t" + line.rstrip())

main()
