#!/usr/bin/env python3
import sys
import time
from pathlib import Path
from toolz.curried import *

import numpy as np
from scipy.spatial.distance import pdist

from joblib import Parallel, delayed

from Bio.PDB import PDBParser

DATA_FILE = Path("/home/algor/Work/proteins/data/raw_chains.txt")

stime = time.time()
print(f"Start time: {stime}", file=sys.stderr)

def compute_vectors(line):
    parts = line.split("\t", 4)
    if parts[3] == '0':
        return parts[:4], np.zeros(0), np.zeros(0)
    vector = np.fromstring(parts[4].replace("\t", ","), dtype=np.float64, sep=",").reshape(-1, 3)
    N = vector.shape[0]
    pdists = pdist(vector)
    pdists_r = pdist(np.flip(vector, axis=0))
    pdists_str = ",".join(np.char.mod('%.4f', pdists))
    pdists_r_str = ",".join(np.char.mod('%.4f', pdists_r))
    return "\t".join(parts[:4]) + "\t" + pdists_str + "\t" + pdists_r_str


BATCH_SIZE = 1000
with open(DATA_FILE, "rt") as fin:
    for itr, batch in enumerate(partition_all(BATCH_SIZE, fin)):
        print(f"{itr * BATCH_SIZE} / 850069", file=sys.stderr)
        for output in Parallel(n_jobs=5, verbose=0)(delayed(compute_vectors)(l) for l in batch):
            print(output)

ftime = time.time()
print(f"Finish time: {ftime}, elapsed {ftime-stime}.", file=sys.stderr)
