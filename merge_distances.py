#!/usr/bin/env python3
import time
import argparse
from pathlib import Path
from itertools import zip_longest


def directory(string):
    path = Path(string)
    if path.is_dir():
        return path
    else:
        raise NotADirectoryError(string)

def file(string):
    path = Path(string)
    if path.is_file():
        return path
    else:
        raise RuntimeError(string)

def main():
    parser = argparse.ArgumentParser(description='Merge distance files.')
    parser.add_argument('length_list', type=file)
    parser.add_argument('input_dir', type=directory)
    args = parser.parse_args()

    stime = time.time()
    with open(args.length_list, "rt") as fin:
        lengths = [s.strip() for s in fin]

    for i, l in enumerate(lengths):
        print(f"[{i} / {len(lengths)}] {l}")
        if l == '1': continue
        fdirect = args.input_dir / f"{l}.direct.res"
        fsorted = args.input_dir / f"{l}.sorted.res"
        fboth = args.input_dir / f"{l}.both.res"
        assert fdirect.is_file()
        assert fsorted.is_file()
        assert fboth.is_file()

        with open(fdirect, "rt") as fin_direct, open(fsorted, "rt") as fin_sorted, open(fboth, "rt") as fin_both,\
             open(args.input_dir / f"{l}.all.res", "wt") as fout:
            for directl, sortedl, bothl in zip_longest(fin_direct, fin_sorted, fin_both):
                dprefix, dd = directl.rsplit('\t', 1)
                sprefix, sd = sortedl.rsplit('\t', 1)
                bprefix, bd = bothl.rsplit('\t', 1)
                assert dprefix == sprefix == bprefix
                dd, sd, bd = dd.strip(), sd.strip(), bd.strip()
                print(f"{dprefix}\t{dd}\t{sd}\t{bd}", file=fout)
    print("Elapsed:", time.time() - stime)

main()
