#!/usr/bin/env python3
import sys
import gzip
import time
import argparse
import numpy as np
from pathlib import Path
from toolz.curried import *
from joblib import Parallel, delayed
from scipy.spatial import KDTree

from Bio.PDB import PDBParser


def directory(string):
    path = Path(string)
    if path.is_dir():
        return path
    else:
        raise NotADirectoryError(string)

def file(string):
    path = Path(string)
    if path.is_file():
        return path
    else:
        raise RuntimeError(string)

def get_chain(database, pdb, model, chain):
    parser = PDBParser(QUIET=True)
    with gzip.open(database / pdb[1:3] / f"pdb{pdb}.ent.gz", "rt") as fin:
        struct = parser.get_structure(pdb, fin)

    for m in struct:
        model_id = m.get_id()
        if str(model_id) != model: continue

        for c in m:
            chain_id = c.get_id()
            if str(chain_id) != chain: continue

            error = False
            chain_atoms = []
            for resid in c:
                # we are looking for only ATOMs not HEATOMs
                if resid.get_id()[0] != ' ': continue
                main_atom = None
                for atom in resid:
                    if atom.get_id() == 'CA':
                        #assert not atom.is_disordered()
                        if main_atom is not None:
                            error = True
                            break
                        main_atom = atom
                if error or main_atom is None:
                    error = True
                    break
                chain_atoms.append(resid.get_resname())
            if error:
                raise RuntimeError("error while processing chain")
            return np.array(chain_atoms)
    raise RuntimeError("chain not found")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('vectors_file', type=file)
    parser.add_argument('database_dir', type=directory)
    args = parser.parse_args()

    with open(args.vectors_file, "rt") as fin:
        for idx, l in enumerate(fin):
            pdb, model, ch, n, vec = l.split("\t", 4)
            if int(n) <= 20: continue
            vec = np.fromstring(vec.replace("\t", ","), dtype=np.float64, sep=",").reshape(-1, 3)
            kdtree = KDTree(vec)
            chain = get_chain(args.database_dir, pdb, model, ch)

            for aa, p in zip(chain, vec):
                dists, closest = kdtree.query(p, 11)
                dists = dists[1:]
                closest = closest[1:]
                closest_names = chain[closest]

                dists_str = "\t".join(np.char.mod('%.4f', dists))
                closest_names_str = "\t".join(closest_names)
                print(f"{aa}\t{dists_str}\t{closest_names_str}")

            print(pdb, model, ch, file=sys.stderr)


main()
