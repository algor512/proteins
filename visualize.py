#!/usr/bin/env python3
import sys
import time
import argparse
from pathlib import Path
import numpy as np
import pandas as pd
from toolz.curried import *

import plotly.graph_objects as go

def file(string):
    path = Path(string)
    if path.is_file():
        return path
    else:
        raise RuntimeError(string)

def create_plot(df, visible):
    if len(df.shape) > 1:
        x, y, z, txts = df.v1.values, df.v2.values, df.v3.values, list(df.pdb + " " + df.model + " " + df.chain)
    else:
        x, y, z, txts = [df.v1], [df.v2], [df.v3], [df.pdb + " " + df.model + " " + df.chain]

    return go.Scatter3d(
        x=x, y=y, z=z,
        mode='markers', marker=dict(size=1, color='#fd3216', opacity=0.8),
        text=txts,
        hovertemplate="%{text} (%{x}, %{y}, %{z})<extra></extra>",
        visible=visible
    )

def mask(idx, N):
    a = [False] * N
    a[idx] = True
    return a

def main():
    parser = argparse.ArgumentParser(description='Visualize data.')
    parser.add_argument('data', type=file)
    args = parser.parse_args()

    data = pd.read_csv(args.data, sep='\t', names=["pdb", "model", "chain", "len", "v1", "v2", "v3"], dtype={"model": str})\
        .sort_values("len").set_index("len")
    unique_len = list(data.index.unique())

    fig = go.Figure()
    for idx, l in enumerate(unique_len):
        fig.add_trace(create_plot(data.loc[l, :], visible=(idx == 0)))

    fig.update_layout(updatemenus=[go.layout.Updatemenu(
        active=0,
        buttons=[
            dict(label=str(l), method="update", args=[{"visible": mask(idx, len(unique_len))}, {"title": "Chains of length " + str(l)}])
            for idx, l in enumerate(unique_len)
        ]
    )], title="Chains of length " + str(unique_len[0]))
    fig.write_html(sys.stdout)


main()
